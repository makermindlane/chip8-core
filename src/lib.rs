use rand::random;

pub const SCREEN_WIDTH: usize = 64;
pub const SCREEN_HEIGHT: usize = 32;

const RAM_SIZE: usize = 4096;
const NUM_REGS: usize = 16;
const NUM_KEYS: usize = 16;
const STACK_SIZE: usize = 16;

const START_ADDR: u16 = 0x200;

const FONTSET_SIZE: usize = 80;
const FONTSET: [u8; FONTSET_SIZE] = [
    0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
    0x20, 0x60, 0x20, 0x20, 0x70, // 1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
    0x90, 0x90, 0xF0, 0x10, 0x10, // 4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
    0xF0, 0x10, 0x20, 0x40, 0x40, // 7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
    0xF0, 0x90, 0xF0, 0x90, 0x90, // A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
    0xF0, 0x80, 0x80, 0x80, 0xF0, // C
    0xE0, 0x90, 0x90, 0x90, 0xE0, // D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
    0xF0, 0x80, 0xF0, 0x80, 0x80, // F
];

pub struct Emulator {
    pc: u16,
    ram: [u8; RAM_SIZE],
    screen: [bool; SCREEN_WIDTH * SCREEN_HEIGHT],
    v: [u8; NUM_REGS],
    i: u16,
    sp: u16,
    stack: [u16; STACK_SIZE],
    keys: [bool; NUM_KEYS],
    dt: u8,
    st: u8,
}

impl Emulator {
    pub fn new() -> Self {
        let mut new_emu = Self {
            pc: START_ADDR,
            ram: [0; RAM_SIZE],
            screen: [false; SCREEN_WIDTH * SCREEN_HEIGHT],
            v: [0; NUM_REGS],
            i: 0,
            sp: 0,
            stack: [0; STACK_SIZE],
            keys: [false; NUM_KEYS],
            dt: 0,
            st: 0,
        };
        new_emu.ram[..FONTSET_SIZE].copy_from_slice(&FONTSET);

        new_emu
    }

    pub fn reset(&mut self) {
        self.pc = START_ADDR;
        self.ram = [0; RAM_SIZE];
        self.ram[..FONTSET_SIZE].copy_from_slice(&FONTSET);
        self.screen = [false; SCREEN_WIDTH * SCREEN_HEIGHT];
        self.v = [0; NUM_REGS];
        self.i = 0;
        self.sp = 0;
        self.stack = [0; STACK_SIZE];
        self.dt = 0;
        self.st = 0;
    }

    pub fn tick(&mut self) {
        let op = self.fetch();
        // Decode & execute
        self.execute(op);
    }

    pub fn tick_timers(&mut self) {
        if self.dt > 0 {
            self.dt -= 1;
        }

        if self.st > 0 {
            if self.st == 1 {
                // beep
            }
            self.st -= 1;
        }
    }

    pub fn get_display(&self) -> &[bool] {
        &self.screen
    }

    pub fn keypress(&mut self, idx: usize, pressed: bool) {
        self.keys[idx] = pressed;
    }

    pub fn load(&mut self, data: &[u8]) {
        let start = START_ADDR as usize;
        let end = start + data.len();
        self.ram[start..end].copy_from_slice(data);
    }

    fn fetch(&mut self) -> u16 {
        let higher_byte = self.ram[self.pc as usize] as u16;
        let lower_byte = self.ram[(self.pc + 1) as usize] as u16;
        let op = higher_byte << 8 | lower_byte;
        self.pc += 2;

        op
    }

    fn execute(&mut self, op: u16) {
        let digit1 = (op & 0xf000) >> 12;
        let digit2 = (op & 0x0f00) >> 8;
        let digit3 = (op & 0x00f0) >> 4;
        let digit4 = op & 0x000f;

        match (digit1, digit2, digit3, digit4) {
            // NOP
            (0, 0, 0, 0) => return,
            // CLS
            (0, 0, 0xe, 0) => self.screen = [false; SCREEN_HEIGHT * SCREEN_WIDTH],
            // RET
            (0, 0, 0xe, 0xe) => {
                let ret_addr = self.pop();
                self.pc = ret_addr;
            }
            // JMP NNN
            (1, _, _, _) => {
                self.pc = op & 0xfff;
            }
            // CALL NNN
            (2, _, _, _) => {
                self.push(self.pc);
                self.pc = op & 0xfff;
            }
            // SKIP VX == NN
            (3, _, _, _) => {
                let x = digit2 as usize;
                let vx = self.v[x];
                let nn = (op & 0x00ff) as u8;
                if vx == nn {
                    self.pc += 2;
                }
            }
            // SKIP VX != NN
            (4, _, _, _) => {
                let x = digit2 as usize;
                let vx = self.v[x] as u8;
                let nn = (op & 0x00ff) as u8;
                if vx != nn {
                    self.pc += 2;
                }
            }
            // SKIP VX == VY
            (5, _, _, 0) => {
                let x = digit2 as usize;
                let y = digit3 as usize;
                let vx = self.v[x];
                let vy = self.v[y];
                if vx == vy {
                    self.pc += 2;
                }
            }
            // VX = NN
            (6, _, _, _) => {
                let x = digit2 as usize;
                let nn = (op & 0x00ff) as u8;
                self.v[x] = nn;
            }
            // VX += NN
            (7, _, _, _) => {
                let x = digit2 as usize;
                let nn = (op & 0x00ff) as u8;
                self.v[x] = self.v[x].wrapping_add(nn);
            }
            // VX = VY
            (8, _, _, 0) => {
                let x = digit2 as usize;
                let y = digit3 as usize;
                self.v[x] = self.v[y];
            }
            // VX |= VY
            (8, _, _, 1) => {
                let x = digit2 as usize;
                let y = digit3 as usize;
                self.v[x] |= self.v[y];
            }
            // VX &= VY
            (8, _, _, 2) => {
                let x = digit2 as usize;
                let y = digit3 as usize;
                self.v[x] &= self.v[y];
            }
            // VX ^= VY
            (8, _, _, 3) => {
                let x = digit2 as usize;
                let y = digit3 as usize;
                self.v[x] ^= self.v[y];
            }
            // VX += VY
            (8, _, _, 4) => {
                let x = digit2 as usize;
                let y = digit3 as usize;

                let (val, carry) = self.v[x].overflowing_add(self.v[y]);
                let vf = if carry { 1 } else { 0 };

                self.v[x] = val;
                self.v[0xf] = vf;
            }
            // VX -= VY
            (8, _, _, 5) => {
                let x = digit2 as usize;
                let y = digit3 as usize;

                let (val, borrow) = self.v[x].overflowing_sub(self.v[y]);
                let vf = if borrow { 0 } else { 1 };

                self.v[x] = val;
                self.v[0xf] = vf;
            }
            // VX >>= 1
            (8, _, _, 6) => {
                let x = digit2 as usize;
                let lsb = self.v[x] & 1;
                self.v[x] >>= 1;
                self.v[0xf] = lsb;
            }
            // VX = VY - VX
            (8, _, _, 7) => {
                let x = digit2 as usize;
                let y = digit3 as usize;

                let (val, borrow) = self.v[y].overflowing_sub(self.v[x]);
                let vf = if borrow { 0 } else { 1 };

                self.v[x] = val;
                self.v[0xf] = vf;
            }
            // VX <<= 1
            (8, _, _, 0xe) => {
                let x = digit2 as usize;
                // let msb = (self.v[x] >> 7) & 1; // but what's the difference?
                let msb = self.v[x] >> 7;
                self.v[x] >>= 1;
                self.v[0xf] = msb;
            }
            // SKIP VX != VY
            (9, _, _, 0) => {
                let x = digit2 as usize;
                let y = digit3 as usize;
                let vx = self.v[x];
                let vy = self.v[y];
                if vx != vy {
                    self.pc += 2;
                }
            }
            // I = NNN
            (0xa, _, _, _) => {
                let nnn = op & 0xfff;
                self.i = nnn;
            }
            // JMP V0 + NNN
            (0xb, _, _, _) => {
                let nnn = op & 0xfff;
                self.pc = (self.v[0] as u16) + nnn;
            }
            // VX = rand() & NN
            (0xc, _, _, _) => {
                let x = digit2 as usize;
                let rng: u8 = random();
                let nn = (op & 0xff) as u8;
                self.v[x] = rng & nn;
            }
            // DRAW
            (0xd, _, _, _) => {
                let x = self.v[digit2 as usize] as u16;
                let y = self.v[digit3 as usize] as u16;
                let rows = digit4;
                let mut flipped = false;
                for row in 0..rows {
                    let addr = self.i + row;
                    let pixels = self.ram[addr as usize];
                    for pixel in 0..8 {
                        if pixels & (0b1000_0000 >> pixel) != 0 {
                            let pixel_x = (pixel + x) as usize % SCREEN_WIDTH;
                            let pixel_y = (row + y) as usize % SCREEN_HEIGHT;
                            let idx = pixel_x + SCREEN_WIDTH * pixel_y;
                            flipped |= self.screen[idx];
                            self.screen[idx] ^= true;
                        }
                    }
                }
                if flipped {
                    self.v[0xf] = 1;
                } else {
                    self.v[0xf] = 0;
                }
            }
            // SKIP KEY PRESS
            (0xe, _, 9, 0xe) => {
                let x = digit2 as usize;
                let vx = self.v[x];
                let key = self.keys[vx as usize];
                if key {
                    self.pc += 2;
                }
            }
            // SKIP KEY RELEASE
            (0xe, _, 0xa, 1) => {
                let x = digit2 as usize;
                let vx = self.v[x];
                let key = self.keys[vx as usize];
                if !key {
                    self.pc += 2;
                }
            }
            // VX = DT
            (0xf, _, 0, 7) => {
                let x = digit2 as usize;
                self.v[x] = self.dt;
            }
            // WAIT KEY
            (0xf, _, 0, 0xa) => {
                let x = digit2 as usize;
                let mut pressed = false;
                for (i, &key) in self.keys.iter().enumerate() {
                    if key {
                        self.v[x] = i as u8;
                        pressed = true;
                        break;
                    }
                }
                if !pressed {
                    // Redo opcode
                    self.pc -= 2;
                }
            }
            // DT = VX
            (0xf, _, 1, 5) => {
                self.dt = self.v[digit2 as usize];
            }
            // ST = VX
            (0xf, _, 1, 8) => {
                self.st = self.v[digit2 as usize];
            }
            // I += VX
            (0xf, _, 1, 0xe) => {
                self.i = self.i.wrapping_add(self.v[digit2 as usize] as u16);
            }
            // I = FONT
            (0xf, _, 2, 9) => {
                let x = digit2 as usize;
                let c = self.v[x] as u16;
                self.i = c * 5;
            }
            // BCD
            (0xf, _, 3, 3) => {
                let x = digit2 as usize;
                let vx = self.v[x] as f32;
                // Fetch the hundreds digit by dividing by 100 and tossing the decimal
                let hundreds = (vx / 100.0).floor() as u8;
                // Fetch the tens digit by dividing by 10, tossing the ones digit and the decimal
                let tens = ((vx / 10.0) % 10.0).floor() as u8;
                // Fetch the ones digit by tossing the hundreds and the tens
                let ones = (vx % 10.0) as u8;
                self.ram[self.i as usize] = hundreds;
                self.ram[(self.i + 1) as usize] = tens;
                self.ram[(self.i + 2) as usize] = ones;
            }
            // STORE V0 - VX
            (0xf, _, 5, 5) => {
                let x = digit2 as usize;
                let i = self.i as usize;
                for idx in 0..=x {
                    self.ram[i + idx] = self.v[idx];
                }
            }
            // LOAD V0 - VX
            (0xf, _, 6, 5) => {
                let x = digit2 as usize;
                let i = self.i as usize;
                for idx in 0..=x {
                    self.v[idx] = self.ram[i + idx];
                }
            }
            (_, _, _, _) => unimplemented!("Unimplemented opcode: {}", op),
        }
    }

    fn push(&mut self, val: u16) {
        self.stack[self.sp as usize] = val;
        self.sp += 1;
    }

    fn pop(&mut self) -> u16 {
        self.sp -= 1;
        self.stack[self.sp as usize]
    }
}

// pub fn add(left: usize, right: usize) -> usize {
//     left + right
// }

// #[cfg(test)]
// mod tests {
//     use super::*;

//     #[test]
//     fn it_works() {
//         let result = add(2, 2);
//         assert_eq!(result, 4);
//     }
// }
